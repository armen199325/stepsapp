app = {
    Steps : function (newSettings) {
        this.numberOfSections = $(".contents").length;
        $(".content").css({"width" : 100 * this.numberOfSections + "%"});
        $(".contents").css({"width": 100 / this.numberOfSections + "%"});
        this.currentPage = 0;
        this.endPosition = null;
        this.animateFinished = true;
        this.position = null;
        this.interv = null;
        this.show = $(".show");
        this.width = this.show.width();
        // this.height = this.show.height();
        this.newPos = 1;
        this.speed = null;
        this.settings = {
            finishButton: false,
            stepValidation: true,
            acceleration: 10,
            startSpeed: 0.1,
            noValidatePages: [],
        };
        let that = this;
        $.extend(this.settings, newSettings);
        this.previous = function (){
            if(this.animateFinished){
                this.enableNextButton();
                if(this.currentPage == 1){
                    this.disablePrevButton();
                }
                if(this.currentPage == this.numberOfSections - 1){
                    this.finishToNext();
                };
                this.animateFinished = false;
                this.position = - parseInt($(".content").css("left"));
                clearInterval(this.interv);
                this.endPosition = (this.currentPage - 1) * this.width;
                this.interv = setInterval(function(){
                    if(that.newPos > that.endPosition){
                        that.speed = (Math.sin(3.14 / that.width * (that.position - that.newPos )) + that.settings.startSpeed) * that.settings.acceleration;
                        that.newPos -= that.speed;
                        $(".content").css("left", - that.newPos + "px");
                    }else {
                        that.animateFinished = true;
                        that.currentPage -= 1;
                        clearInterval(that.interv);
                    }
                }, 10);
            }
        };
        this.next = function(){
            if(this.animateFinished && this.currentPage != this.numberOfSections - 1){
                if(this.settings.stepValidation){

                    if(this.validated[this.currentPage + 1]){
                        this.enableNextButton();
                    } else {
                        this.disableNextButton();
                    }
                };
                if(this.currentPage == this.numberOfSections - 2 && this.settings.finishButton){
                    this.finishButton();
                };
                if(this.currentPage == 0){
                    this.enablePrevButton()
                };
                this.animateFinished = false;
                this.position = - parseInt($(".content").css("left"));
                clearInterval(this.interv);
                this.endPosition = (this.currentPage + 1) * this.width;
                this.interv = setInterval(function(){
                    if(that.newPos < that.endPosition){
                        that.speed = (Math.sin(3.14 / that.width * (that.newPos - that.position)) + that.settings.startSpeed) * that.settings.acceleration;
                        that.newPos += that.speed;
                        $(".content").css("left", - that.newPos + "px");
                        console.log(that.speed);
                    }else {
                        that.animateFinished = true;
                        that.currentPage += 1;
                        clearInterval(that.interv);
                    };
                }, 10);
            };
        };
        this.resize = function(){
            this.width = this.show.width();
            this.height = this.show.height();
            $(".content").css("left", - this.currentPage * this.width + "px");
        };

        this.disableNextButton = function(){
            $("#next").prop("disabled", true);
        };

        this.enableNextButton = function(){
            $("#next").prop("disabled", false);
        };

        this.disablePrevButton = function(){
            $("#prev").prop("disabled", true)
        };

        this.enablePrevButton = function (){
            $("#prev").prop("disabled", false);
        };

        this.finishButton = function(){
            $("#next").text("Finish").prop("type", "submit");
        };
        this.finishToNext = function(){
            $("#next").text("Next Page").prop("type", "none");
        };
        this.validated = [];
        for(let i = 0; i < this.numberOfSections; i++){
            this.validated[i] = false;
        }
        for(i = 0; i < this.settings.noValidatePages.length; i++){
            this.validated[this.settings.noValidatePages[i] - 1] = true;
        };
    },
}
let steps = new app.Steps({finishButton: true, noValidatePages:[1, 2], stepValidation: true, acceleration:20});
if(!steps.validated[0] && steps.settings.stepValidation){
    steps.disableNextButton();
}
$(window).resize(function () {
    steps.resize();
});

$("#next").click(function () {
    steps.next();
});
$("#prev").click(function () {
    steps.previous();
});